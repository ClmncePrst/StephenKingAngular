export type Book = {
    key: string,
    title: string,
    covers: number[],
    description: string | { value: string }
}