import { Component } from '@angular/core';
import { BookapiService } from '../bookapi.service';
import { Book } from '../Book';
import { RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CoverUrlPipe } from '../pipe/cover-url.pipe';

@Component({
  selector: 'app-book-list',
  standalone: true,
  imports: [RouterLink, CommonModule, CoverUrlPipe],
  templateUrl: './book-list.component.html',
  styleUrl: './book-list.component.css'
})
export class BookListComponent {
  bookList: Book[] = []

  constructor(private bookapiService: BookapiService) { }

  ngOnInit() {
    this.bookapiService.getAllBooks()
      .subscribe((response) => this.bookList = response);
  }
}
