import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookapiService } from '../bookapi.service';
import { Book } from '../Book';
import { FormatDescriptionPipe } from '../pipe/format-description.pipe';


@Component({
  selector: 'app-book-details',
  standalone: true,
  imports: [FormatDescriptionPipe],
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.css'
})
export class BookDetailsComponent {
  bookKey: string | null = null;
  bookDetails: Book | null = null;

  constructor(
    private route: ActivatedRoute,
    private bookApiService: BookapiService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.bookKey = params.get('bookID');

      if (this.bookKey !== null) {
        this.bookApiService.getBookByKey(this.bookKey)
          .then((response) => this.bookDetails = response);
      }

    });
  }
}