import { Injectable } from '@angular/core';
import { Book } from './Book';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';


type ResponseAPI = {
  entries: Book[]
}

@Injectable({
  providedIn: 'root'
})
export class BookapiService {
  constructor(private http: HttpClient) { }

  getAllBooks(): Observable<Book[]> {
    return this.http.get<ResponseAPI>('https://openlibrary.org/authors/OL2162284A/works.json')
      .pipe(
        map((response) => response.entries)
      )
  }

  getBookByKey(bookKey: string): Promise<Book> {
    return fetch('https://openlibrary.org' + bookKey + '.json')
      .then((response) => response.json())
      .then((response: Book) => response);
  }
};
