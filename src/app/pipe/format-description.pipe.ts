import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDescription',
  standalone: true
})
export class FormatDescriptionPipe implements PipeTransform {

  transform(description: string | { value: string }): string {
    if (!description) {
      return "Aucune description disponible pour le moment";
    } else if (typeof description == "string") {
      return description;
    } else {
      return description.value;
    }
  }
}
