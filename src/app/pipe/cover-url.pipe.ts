import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coverUrl',
  standalone: true
})
export class CoverUrlPipe implements PipeTransform {

  transform(coverID: number[]): string {
    if (coverID && coverID.length > 0 && coverID[0] != -1) {
      const coverIDs = coverID[0];
      return "https://covers.openlibrary.org/b/id/" + coverIDs + "-M.jpg";
    } else {
      return "https://placehold.co/200x300";
    }

  }

}
